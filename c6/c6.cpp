

#include <iostream>
#include <cstdlib>
#include <ctime> 
#include <string>

using namespace std;

class Character {
public:
	string name;
	string pass;
	int exp;
	int loc1;
	int loc2;
	string inv1;
	string inv2;
	string inv3;
	string inv4;
	void print(string msg);
};





void main() {

	srand(time(0));

	Character lillith, renexor, garfas;

	lillith.exp = 4563;
	lillith.name = "Lillith";
	lillith.pass = "password1";
	lillith.loc1 = 24000;
	lillith.loc2 = 320;
	lillith.inv1 = "Sword";
	lillith.inv2 = "shield";
	lillith.inv3 = "ring";
	lillith.inv4 = "potion";
	lillith.print("Player info -");
	renexor.exp = 56721;
	renexor.name = "renexor";
	renexor.pass = "password2";
	renexor.loc1 = 32453;
	renexor.loc2 = 4256;
	renexor.inv1 = "Axe";
	renexor.inv2 = "Helmet";
	renexor.inv3 = "Scroll of fire";
	renexor.inv4 = "Stick";
	renexor.print("Player info -");
	garfas.exp = 345;
	garfas.name = "garfas";
	garfas.pass = "password3";
	garfas.loc1 = 17;
	garfas.loc2 = 45987;
	garfas.inv1 = "bow";
	garfas.inv2 = "arrows";
	garfas.inv3 = "amulet";
	garfas.inv4 = "Staff";
	garfas.print("Player info -");

	system("pause");
}
void Character::print(string msg) {
	cout << msg << endl;
	cout << "Name: " << name << endl;
	cout << "Password: " << pass << endl;
	cout << "Experience: " << exp << endl;
	cout << "Position: " << loc1 << ", " << loc2 << endl;
	cout << "Inventory:" << endl;
	cout << inv1 << endl;
	cout << inv2 << endl;
	cout << inv3 << endl;
	cout << inv4 << endl;
	cout << "\n";
}